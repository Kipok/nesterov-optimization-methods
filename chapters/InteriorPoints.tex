﻿% Interior Points

\chapter{Методы внутренней точки}\label{InteriorPoints}

%----------------------------------------------------------------------------------------

    \section{Метод Ньютона для ограничений-равенств}\label{BasicPart}
        Попробуем теперь применить полученные в прошлом разделе факты для обобщения метода Ньютона на случай ограничений-равенств. Пусть мы решаем задачу выпуклой оптимизации:
        \begin{align*}
            &\hspace{-1pt}\min{f(x)} \\
            &\text{при}\ a^T_ix - b_i = 0 \Leftrightarrow Ax = b
        \end{align*}
        Тогда, записывая условия Каруша-Куна-Таккера (ККТ) мы получим следующую эквивалентную задачу (так как у нас нет ограничений-неравенств, то условие Слейтера выполняется):
        \begin{align*}
            Ax &= b\\
            \nabla f(x) + A^Tv &= 0
        \end{align*}
        Вспомним, что для решения безусловной задачи методом Ньютона, мы заменяли на каждом шаге функцию $f(x)$ ее квадратичным приближением. Попробуем сделать тоже самое для нашей задачи. В точке
        $x\in D(f)$ решим следующую задачу (относительно $h$):
        \begin{align*}
            &\hspace{-1pt}\min{\left(f(x) + \nabla f(x)^T h + \frac{1}{2}h^T\nabla^2f(x)h\right)} \\
            &\text{при}\ A(x + h) = b
        \end{align*}
        Для этой задачи мы вполне можем найти решение аналитически из ККТ-условий:
        \begin{align*}
            \begin{bmatrix} \nabla^2f(x) & A^T \\ A & 0 \end{bmatrix}
            \begin{bmatrix} h \\ v \end{bmatrix} = \begin{bmatrix} -\nabla f(x) \\ 0 \end{bmatrix}
        \end{align*}
        Тут стоит заметить, что точка $x$ должна удовлетворять условию $Ax = b$, и необходимо, чтобы $\nabla^2f(x) \succ 0$ (например для этого можно потребовать строгую выпуклость $f(x)$). Таким образом, мы нашли такое направление $h$ для метода Ньютона, которое минимизирует нашу функцию вдоль заданных ограничительных гиперплоскостей. В~\cite{Boyd2004} приводится доказательство того, что этот метод можно переформулировать, как обычный метод Ньютона для несколько другой, уже безусловной, задачи, что означает, что оценки его скорости сходимости не изменились.
    \section{Метод Логарифмических барьеров}\label{NewtonMethodAgain}
        Вернемся теперь к нашей исходной выпуклой оптимизационной задаче:
        \begin{align*}
            \min\ &f(x) \\
            \text{при}\ &f_i(x) \le 0, i=1..m\\
            &Ax = b
        \end{align*}
        Попробуем обобщить полученный метод и на случай неравенств. Для этого, нам нужно представить нашу задачу (хотя бы приближенно) в виде оптимизационной задачи с ограничениями только вида равенств. Например, это можно сделать следующим образом:
        \begin{align*}
            &\hspace{-1pt}\min{f(x) + \phi(x)}\\
            &\text{при}\ Ax = b
        \end{align*}
        Здесь $\phi(x)$ — барьерная функция. Барьерной функцией будем называть любую функцию \[\phi(x) = \left\{ \begin{aligned} +\infty, \exists\ i: f_i(x) > 0 \\ \text{$\phi(x)$ — гладкая}, \forall i f_i(x) \le 0 \end{aligned} \right.\]
        Ясно, что если бы мы могли выбрать в качестве $\phi(x)$ индикаторную функцию $I_{-}$, то получилась бы абсолютно эквивалентная задача. Но, к сожалению, в этом случае мы не можем гарантировать дифференцируемость наших функций (хотя, конечно, задача останется выпуклой). Поэтому, рассмотрим один из самых простых вариантов барьерной функции:
        \[\phi(x) = -\frac{1}{t}\displaystyle\sum_{i = 1}^{m}{\ln{(-f_i)}}\]
        Эта функция называется логарифмическим барьером. Параметр $t$, отвечает за точность приближения исходной задачи. Соответственно, мы получаем следующую приближенную задачу:
       \begin{align*}
            &\hspace{-1pt}\min\ \left(f(x) - \frac{1}{t}\displaystyle\sum_{i = 1}^{m}{\ln{(-f_i)}}\right) \Leftrightarrow \min\ \left(tf(x) - \displaystyle\sum_{i = 1}^{m}{\ln{(-f_i)}}\right)\\
            &\text{при}\ Ax = b
        \end{align*}
        Множество $\{x^*(t)\}$ решений этой задачи в зависимости от $t$ называется центральным путем. Заметим, что из ККТ-условий: $t\nabla f(x^*(t)) + \nabla \phi(x^*(t)) + A^Tv = 0$. Значит,
         \[g(\lambda^*, v^*) = f(x^*) - \frac{m}{t} \left[ \lambda^* = \left[\frac{1}{tf_1(x^*(t))} \ldots \frac{1}{tf_m(x^*(t))}\right]^T,\ v^* = \frac{v}{t}\right]\]
         Это утверждение непосредственно получается из определения наших функций ($m$ — количество ограничений неравенств). Отсюда следует замечательное свойство:
         \[f(x^*(t)) - f^* \le \frac{m}{t}\]
         Это означает, что при $t \ge \frac{m}{\epsilon}$ мы получим, что решение приближенной задачи отличается от решения исходной задачи не больше, чем на $\epsilon$. Но, практика показывает, что метод Ньютона для решения приближенной задачи с большим $t$ работает достаточно долго, если только у нас нет хорошей начальной точки $x_0$ (близкой к области квадратичной сходимости метода Ньютона). Поэтому был предложен следующий алгоритм:
        \begin{enumerate}
            \item[]\hspace{-13pt}\line(1, 0){403}
            \item Выбираем начальные значения $t_0 > 0, \mu > 1, x_0 \in D: \forall\ i f_i(x_0) < 0$ и нужную точность $\epsilon$.
            \item Посчитаем $x_{k+1}(t_k)$, как решение приближенной задачи с начальной точкой $x_k$.
            \item Положим $t_{k+1} = \mu t_{k}$.
            \item Проверяем, получена ли достаточная точность: $\frac{m}{t_k} < \epsilon$
            \item Завершаемся, либо переходим на шаг 2.
            \item[]\hspace{-13pt}\line(1, 0){403}
        \end{enumerate}
        То есть мы каждый раз решаем все более и более точную приближенную задачу, используя ответ на предыдущую, как начальную точку.
        Ясно, что для достижения необходимой точности нам нужно сделать $k \ge \frac{\ln{(m/(\epsilon t_0))}}{\ln\mu}$ итераций. Причем в данном случае эта оценка не содержит никаких неизвестных констант. Правда на каждой итерации мы будем решать приближенную задачу методом Ньютона, для которого точных оценок мы обычно не знаем.

        Возникает также вопрос, как нам найти подходящую точку $x_0$? Для этого можно решить следующую задачу:
        \begin{align*}
            \min\ &t \\
            \text{при}\ &f_i(x) \le t, i=1..m\\
            &Ax = b
        \end{align*}

        Эту задачу можно решать уже описанным методом (для нее достаточно выбрать $ x \in D,\ s > \max f_i(x)$ как начальные значения). Если ее оптимальное решение $t* < 0$, то мы нашли нужную нам точка $x_0$.

        Стоит заметить, что подобные методы внутренней точки, в общем случае, обладает несколькими недостатками. Во-первых, если мы хотим, чтобы метод Ньютона на втором шаге нашего алгоритма работал быстро, то мы должны выбирать $\mu$ достаточно маленьким и соответственно делать много итераций. Во-вторых, мы явно не знаем область квадратичной сходимости метода Ньютона, а значит не можем аналитически подобрать параметры оптимально. И, в-третьих, если мы захотим, например, изменить нашу координатную систему (выбрать другой базис), то все оценки кардинально изменяться, так как константы $\mu$ и $L$ из оценок метода Ньютона зависят от обычного скалярного произведения, а значит и от выбранного базиса.

        Все эти проблемы были решены Ю. Нестеровым и А. Немировским. Они предложили ввести несколько более сильное ограничение на наши функции: самосогласованность.