﻿% Interior Points

\chapter{Применение самосогласованных функций в методах внутренней точки}\label{SelfConcordance}

%----------------------------------------------------------------------------------------

        В этом разделе я буду приводить много утверждений без доказательства, так как иначе мой реферат был бы слишком объемным. Более подробные рассуждения не очень сложны и хорошо описаны в ~\cite{Nesterov2004}.
        
        Итак, попробуем вначале разобраться с третьей проблемой: не инвариантностью скорости сходимости метода Ньютона относительно преобразований системы координат. Заметим, что сам метод, вообще говоря инвариантен относительно преобразования $y = Ax$ (это проверяется непосредственно: шаг метода Ньютона в новой системе связан с шагом в старой теми же преобразованиями). Поэтому  нужно заменить наше основное предположение, что $\nabla^2f$ удовлетворяет условию Липшица, при $x \in D(f)$, каким-нибудь свойством, инвариантным относительно преобразования системы координат.

    \section{Самосогласованные функции}\label{BasicSelfConcordance}

        Функция $f : \mathbb{R} \rightarrow \mathbb{R}$ называется самосогласованной, если:
        \[|f'''(x)| \le 2f''(x)^{3/2}\]
        Вместо $2$, в общем-то, может быть любая другая положительная константа. Легко проверить, что нужная нам инвариантность выполняется. Если же функция $f : \mathbb{R}^n \rightarrow \mathbb{R}$, то она называется самосогласованной, если $\forall x \in D(f),\ \forall\ v: f(x + tv)$ — са\-мо\-со\-гла\-со\-ван\-ная функция от $t$. Не сложно проверить, что если $f_1, f_2$ — самосогласованные функции, то следующие функции также самосогласованы:
         \begin{itemize}
            \item $f_1 + f_2$;
            \item $\alpha f_1, \alpha > 1$;
            \item $f_1(Ax + b)$;
            \item $f(x) = \ln(x)$;
            \item $f(x) = x\ln(x) - \ln(x)$;
         \end{itemize}
        \noindent
        Введем также понятие самосогласованного барьера: самосогласованная функция $F$ называется $v-$са\-мо\-со\-гла\-со\-ван\-ным барьером для $D(F)$, если $\nabla F(x)^T\nabla^2F(x)\nabla F(X) \le v, \forall x$. Например функция $F(x) = -\ln{x}$ — $1-$самосогласованный барьер для $\mathbb{R}^{+}$. Также, заметим, что сумма двух самосогласованных барьеров, с параметрами $v_1$ и $v_2$ будет $(v_1 + v_2)$-са\-мо\-со\-гла\-со\-ван\-ным барьером.

    \section{Задачи минимизации — Метод Ньютона}\label{MinTasks}

        Итак, что поменяется, если наша функция $f$ для задачи выпуклой безусловной минимизации будет самосогласованной? Во-первых, у нас появляется следующее достаточное условие существования и единственности решения такой задачи: 
        \[\text{Если $\exists\ x: \lambda_f(x) = \left(\nabla f(x)^T \nabla ^2 f(x)^{-1} \nabla f(x)\right)^{1/2} < 1 \Rightarrow \exists!\ x^*$}\]
        Во-вторых, мы можем оценивать скорость сходимости метода Ньютона через инвариантную относительно преобразования системы координат характеристику $\lambda_f(x)$ (это по-сути длина градиента, для пространства в котором матрица Грама имеет вид обратного Гессиана от нашей функции, который положительно определен, если $f$ — самосогласованная функция). Разобьем стандартный метод Ньютона на два части. Вначале будем использовать, так называемый «демпфированный метод» (это метод Ньютона с параметром $t_k = \frac{1}{1 + \lambda_f(x_k)}$). А затем, после того, как наш метод войдет в область своей квадратичной сходимости примем $t = 1$ (стандартный метод).
        Приведем оценки для обоих частей этого алгоритма. Для демпфированного метода верно, что:
        \[ f(x_{k + 1}) \le f(x_k) - (\lambda(x_k) - \ln{(1 + \lambda(x_k))} \]
        То есть мы можем точно сказать, насколько мы улучшаем наш ответ на каждой итерации. Возникает вопрос, сколько всего нам нужно сделать итераций, чтобы войти в область квадратичной сходимости? Ответ на этот вопрос можно получить из следующего утверждения.

        Пусть $x_k \in D(f), \lambda_f(x_k) < 1$, тогда:
        \[x_{k + 1} = x_k - \nabla^2f(x_k)^{-1}\nabla f(x_k) \in D(f)\]
        \[ \lambda(x_{k + 1}) \le \left(\frac{\lambda(x_k)}{1 - \lambda(x_k)}\right)^2 \]
        Отсюда можно вывести, что область квадратичной сходимости задается следующим образом: $x_k: \lambda(x_k) < \tilde{\lambda} = \frac{3 - \sqrt{5}}{2} \Rightarrow \lambda_f(x_{k + 1}) < 2\lambda_f(x_k)^2$. $\tilde{\lambda}$ — корень уравнения $\frac{\lambda^2}{(1-\lambda)^2} = \lambda$. Получаем, что нам нужно использовать демпфированный метод Ньютона, пока не станет верно $\lambda(x_k) \le \beta < \tilde{\lambda}$, а затем использовать стандартный метод Ньютона. Не очень сложными преобразованиями (подробнее можно посмотреть~\cite{Boyd2004}) можно получить, что если $f$ строго выпуклая и самосогласованная, то
        \[f^* \ge f(x) + \lambda_f(x) + \ln(1 - \lambda_f(x)) \approx f(x) - \frac{\lambda_f(x)^2}{2} \ge f(x) - \lambda_f(x)^2\]
        Соответственно, если $\lambda_f(x_k)^2 \le \epsilon$, то наш алгоритм достиг нужной точности и мы можем завершаться.

    \section{Использование самосогласованных барьеров}\label{BarierMethods}

        Модифицируем теперь описанный выше метод внутренней точки, так чтобы избавиться от всех озвученных проблем.
        Пусть $G$ — выпуклое замкнутое и ограниченное множество в $\mathbb{R}^{n}$), тогда переформулируем нашу задачу в виде:
        \begin{align*}
            &\hspace{-1pt}\min{c^Tx}\\
            &\text{при}\ x \in G
        \end{align*}

        \noindent
        Это всегда можно сделать, так как мы можем рассмотреть множество
        \[G = \left\{(t, x) | x \in D,\ f_i(x) \le 0,\ h_i(x) = 0,\ f(x) \le t\right\}\]
        И просто минимизировать переменную $t$. Просто, в таком виде нам удобнее искать самосогласованные барьеры для нашей задачи (мы всегда можем отдельно найти барьеры для каждого из наших ограничений и просуммировать их).

        Пусть теперь у нас есть $v-$самосогласованный барьер для области $G$ — $F: \nabla^2F(x) \succeq 0$ (подробнее о том, как строить такие барьеры хорошо написано в~\cite{Nemirovsky2004}). Построим опять приближенную задачу:
         \begin{align*}
            &\hspace{-1pt}\min{F_t(x) = tc^Tx + F(x)}\\
            &\text{при}\ x \in G
        \end{align*}
        Мы хотим научиться так выбирать параметры обычного метода внутренней точки, чтобы после каждой итерации оставаться в области квадратичной сходимости модифицированного метода Ньютона: $\lambda_{F_t}(x_k) \le \beta < \tilde{\lambda}$. Следующие утверждения оказываются верными для нашей задачи:
        \[ c^Tx^*(t) - c^* \le \frac{v}{t}\ \text{($c^*$ — оптимальное значение функции для нашей задачи)} \]
        \[ \text{Если $\lambda_{F_{t_k}}(x_k) \le \beta$, то}\ c^Tx^*(t) - c^* \le \frac{1}{t}\left(v + \frac{(\beta + \sqrt{v})\beta}{1 - \beta}\right) \]
        \[ \text{Если $|\gamma| \le \frac{\sqrt{\beta}}{1 + \sqrt{\beta}} - \beta$ и $t_{k + 1} = t_k + \frac{\gamma}{c^T\nabla^2F(x)c}$, тогда $\lambda_{F_{t_{k + 1}}}(x_{k + 1}) \le \beta$}\]
        Это и означает, что при данном выборе $t$ мы будем оставаться в области квадратичной сходимости. Более-менее оптимальными параметрами считаются $\beta = 1/9$ и $\gamma = 5/36$. Соответственно, мы приходим к следующему методу:
        \begin{enumerate}
            \item[]\hspace{-13pt}\line(1, 0){403}
            \item Выбираем начальные значения $t_0 = 0, \beta > 0, \gamma: |\gamma| \le \frac{\sqrt{\beta}}{1 + \sqrt{\beta}} - \beta$ и нужную точность $\epsilon$. Кроме того, выбираем $x_0 \in D(F): \lambda_F(x_0) \le \beta$.
            \item Положим $t_{k + 1} = t_k + \frac{\gamma}{c^T\nabla^2F(x)c}$.

            \item Считаем $x_{k + 1} = x_k - \nabla^2F(x_k)^{-1}(t_{k + 1}c + \nabla F(x_k))$ (для данной вариации алгоритма нам достаточно делать всего лишь одну (!) итерацию метода Ньютона)
            \item Если мы достигли нужной точности: $\epsilon t_k \le v + \frac{(\beta + \sqrt{v})\beta}{1 - \beta}$, то завершаемся, иначе переходим на шаг 2.
            \item[]\hspace{-13pt}\line(1, 0){403}
        \end{enumerate}

        \noindent
        Утверждается, что этот метод сходится за $O\left(\sqrt{v}\ln{\frac{v}{\epsilon}}\right)$ итераций.
%
%        Пусть теперь у нас есть $v-$самосогласованный барьер $F: D(F) = G, \nabla^2F(x) \succeq 0$ (подробнее о том, как строить такие барьеры хорошо написано в~\cite{Nemirovsky2004}). Построим опять приближенную задачу:
%         \begin{align*}
%            &\hspace{-1pt}\min{F_t(x) = tc^Tx + F(x)}\\
%            &\text{при}\ x \in G
%        \end{align*}
%        Тогда, наш модифицированный метод будет выглядеть следующим образом:
%        \begin{enumerate}
%            \item[]\hspace{-13pt}\line(1, 0){403}

%            \item Выбираем начальные значения $t_0 > 0, \gamma > 0, \kappa \in (0, 1), x_0 \in D: \forall\ i f_i(x_0) < 0$ и нужную точность $\epsilon$.
%            \item Воспользуемся для нахождения $x_{k+1}$ демпфированным методом Ньютона с начальной точкой $x_k$ и условием выхода: $\left(\nabla_x F_t(x)^T \nabla_x^2 F_t(x)^{-1} \nabla_xF_t(x)\right)^{1/2} \le \kappa$
%            \item Положим $t_{k + 1} = (1 + \frac{\gamma}{\sqrt{v}})t_k$
%            \item Проверяем, получена ли достаточная точность: $\lambda_t(x)^2 < \epsilon$ ($\lambda_t(x)$ считается от функции $F_t(x)$)
%            \item Завершаемся, либо переходим на шаг 2.
%            \item[]\hspace{-13pt}\line(1, 0){403}
%        \end{enumerate}
%        В~\cite{Nemirovsky2004} показано, что данный метод имеет следующие оценки сходимости:
%        \[ c^Tx_k - f^* \le \frac{2v}{t_0}(1 + \frac{\gamma}{\sqrt{v}})^{-k} \]
%        Кроме того, утверждается, что количество итераций для каждого запуска метода Ньютона не превосходит некоторой константы, зависящей только от $\kappa$ и $\gamma$. И, более того, чтобы достигнуть нужной точности нам нужно сделать не более чем $C\sqrt{v}\ln{(\frac{v}{t_0\epsilon} + 1)}$ итераций в метода Ньютона (то есть суммарно за все его запуски). $C$ зависит только от $\kappa$ и $\gamma$, которые нам известны. Видно, что чем меньше параметр $v$ у используемого барьера, тем лучше сходится наш метод.

        Стоит сказать, что некоторые вопросы по поводу этих методов остались нерассмотренными. Например, как найти нужные начальные точки. Эта задача решается почти также, как и в обычном методе внутренней точки, описанным выше. Мы строим немного другую изначальную оптимизационную задачу и, решая ее, получаем нужные значения. 